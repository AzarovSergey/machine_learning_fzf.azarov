#include <iostream>
#include <string>
#include <ANN.h>

using namespace std;
using namespace ANN;

const string input_file = "../test.txt";
const string output_file = "../network.txt";


int main()
{
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;

	vector<vector<float>> input_data, output_data;

	
	if (!LoadData(input_file, input_data, output_data))
	{
		cout << "Error: not file" << endl;
	}

	auto network = CreateNeuralNetwork();

	if (! network->Load(output_file))
	{
		cout << "Cannot read network from file" << endl;
	}
	
	
	cout << "network type information: ";
	cout << network->GetType() << endl;
	
	for (int i = 0; i < input_data.size(); i++)
	{
		auto output = network->Predict(input_data[i]);
		cout << endl;
		cout << "input:    " << input_data[i][0];
		cout << "\tinput:    " << input_data[i][1];
		cout << "\texpected: " << output_data[i][0];
		cout << "\toutput:   " << output[0]<< endl;
	}
	return 0;
}