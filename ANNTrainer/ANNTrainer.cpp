#include <iostream>
#include <ANN.h>
#include <string>

using namespace std;
using namespace ANN;

const string input_file = "../test.txt";
const string output_file = "../network.txt";

int main()
{
	cout << "Hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;

    vector < size_t > config({ 2, 3, 3, 1 });

	auto network = CreateNeuralNetwork(config);
	
	vector < vector < float > > input_data, output_data;
    
	
	if (!LoadData(input_file, input_data, output_data))
	{
		cout << "Error: not file"<< endl;
	}
	cout << "Network type information: ";
	cout << network->GetType() << endl;
	
	BackPropTraining(network, input_data, output_data,20000,10.e-2,0.1,true);

	if (!network->Save(output_file))
	{
		cout << "Cannot save network to file" << endl;
	}

	return 0;
}