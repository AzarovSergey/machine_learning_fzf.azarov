#define ANNDLL_EXPORTS
#include "ANN.h"
#include "iostream"

using namespace ANN;
using namespace std;

class MyNetwork : public ANeuralNetwork
{

public:
	MyNetwork(
		vector<size_t> config,
		ANeuralNetwork::ActivationType activation_type,
		float scale)
	{
		this->configuration = config;
		this->activation_type = activation_type;
		this->scale = scale;
		if (!config.empty())
			this->RandomInit();

	}

	virtual string ANeuralNetwork::GetType()
	{
		return "Network by Sergey Azarov";
	}

	virtual vector<float> ANeuralNetwork::Predict(vector<float>& input)
	{
	
		if (!is_trained)
		{
			
			runtime_error( "Typay setb");
		}
		
		if (configuration.empty())
		{
			runtime_error( "Config pust");
		}

		if (configuration[0]!= input.size())
		{
			runtime_error("config != input");
		}

		vector < float > prev_out = input;
		vector < float > out;

		for (size_t layer_idx = 0; layer_idx < configuration.size()-1; layer_idx++)
		{
			out.resize(configuration[layer_idx+1],0);
			for (size_t to_idx=0; to_idx<configuration[layer_idx+1];to_idx++)
			{
				for (size_t from_idx=0; from_idx<configuration[layer_idx];from_idx++)
				{
					out[to_idx] += weights[layer_idx][from_idx][to_idx] * prev_out[from_idx];
				}
				out[to_idx] = Activation(out[to_idx]);
			}
			prev_out = out;
		}
		return out;
	}


};


ANNDLL_API std::shared_ptr<ANN::ANeuralNetwork> ANN::CreateNeuralNetwork(
	std::vector<size_t>& configuration,
	ANeuralNetwork::ActivationType activation_type,
	float scale)
{
	return std::make_shared<MyNetwork>(configuration, activation_type, scale);
}


ANNDLL_API float ANN::BackPropTrainingIteration(
	std::shared_ptr<ANN::ANeuralNetwork> ann,
	const std::vector<float>& input,
	const std::vector<float>& output,
	float speed
)
{
	//throw runtime_error("Error");

	float error = 0.0;
	vector<vector<float>> outs(ann->configuration.size());
	outs[0] = input;
	// ������ ������
	
	for (size_t layer_idx = 0; layer_idx < ann->configuration.size() - 1; layer_idx++)
	{
		outs[layer_idx + 1].resize(ann->configuration[layer_idx + 1]);
		for (size_t to_idx = 0; to_idx < ann->configuration[layer_idx + 1]; to_idx++)
		{
			outs[layer_idx + 1][to_idx] = 0.0;
			for (size_t from_idx = 0; from_idx < ann->configuration[layer_idx]; from_idx++)
			{
				outs[layer_idx+1][to_idx] += ann->weights[layer_idx][from_idx][to_idx] * outs[layer_idx][from_idx];
			}
			outs[layer_idx+1][to_idx] = ann->Activation(outs[layer_idx+1][to_idx]);
		}
		
	}

	// ���������� ������ �� ������
	vector<vector<float>> sigmas(ann->configuration.size());
	vector<vector<vector<float>>> dw(ann->configuration.size() - 1);
	sigmas.back().resize(outs.back().size());
	for (size_t out_idx = 0; out_idx < output.size(); out_idx++)
	{
		sigmas.back()[out_idx] = (output[out_idx] - outs.back()[out_idx])*ann->Activation(outs.back()[out_idx]); // ������� �����
		error += (output[out_idx] - outs.back()[out_idx])*(output[out_idx] - outs.back()[out_idx]); // ������� ������ 
	}

	// �������� ��������������� ������ 

	// ��������� ������ ��������� ������ �� ������ ������� � ������� ��������� �� ����������������� �������
	for (size_t layer_idx = ann->configuration.size() - 2; layer_idx + 1 != 0; layer_idx--)
	{
		dw[layer_idx].resize(ann->weights[layer_idx].size());
		sigmas[layer_idx].resize(ann->configuration[layer_idx], 0.0);
		for (size_t from_idx = 0; from_idx < ann->configuration[layer_idx]; from_idx++)
		{
			for (size_t to_idx =0; to_idx < ann->configuration[layer_idx+1]; to_idx++)
			{
				sigmas[layer_idx][from_idx] += sigmas[layer_idx + 1][to_idx] * ann->weights[layer_idx][from_idx][to_idx];
			}
			sigmas[layer_idx][from_idx] *= ann->ActivationDerivative(outs[layer_idx][from_idx]); // �������� ���������� �������� �� ����������� ����������� ����
			dw[layer_idx][from_idx].resize(ann->weights[layer_idx][from_idx].size());
			for (size_t to_idx = 0; to_idx < ann->configuration[layer_idx + 1]; to_idx++)
			{
				dw[layer_idx][from_idx][to_idx] = speed * sigmas[layer_idx + 1][to_idx] * outs[layer_idx][from_idx];
			}
			

			
		}
	}

	// ����������� ����� 
	for (size_t layer_idx = 0; layer_idx < ann->weights.size(); layer_idx++)
	{
		for (size_t from_idx = 0; from_idx < ann->weights[layer_idx].size(); from_idx++)
		{
			for (size_t to_idx = 0; to_idx < ann->weights[layer_idx][from_idx].size(); to_idx++)
			{
				ann->weights[layer_idx][from_idx][to_idx] += dw[layer_idx][from_idx][to_idx];
			}
		}
	}
	return error;

}
ANNDLL_API float ANN::BackPropTraining(
	std::shared_ptr<ANN::ANeuralNetwork> ann,
	std::vector<std::vector<float>>& inputs,
	std::vector<std::vector<float>>& outputs,
	int maxIters,
	float eps,
	float speed,
	bool std_dump
)

	//throw runtime_error("Error");
	{
		ann->RandomInit();


		if (inputs.size() != outputs.size())
		{
			throw runtime_error("Error: inputs != outputs");

		}
		float  error = 0.0;

		int iter_idx = 0;
		size_t sample_number = inputs.size();
		do
		{
			error = 0.0;
			for (size_t sample_idx = 0; sample_idx < sample_number; sample_idx++)
			{
				error += BackPropTrainingIteration(ann, inputs[sample_idx], outputs[sample_idx], speed);

			}
			iter_idx++;
			error = sqrt(error);
			if (std_dump && iter_idx % 100 == 0)
			{
				cout << "Iterations: " << iter_idx << " Erorr: " << error << endl;
			}

			if (error < eps)
			{
				ann->is_trained = true;
			}


		} while (error > eps && iter_idx <= maxIters);
		return error;
	}

