#include <iostream>
#include <FeatureExtraction.h>
#include "Visualisation.h"

using namespace std;
using namespace cv;
using namespace fe;

int main() 
{
	cout << "Hello world!" << endl;
	cout << GetTestString().c_str() << endl;

	auto blob_proc = CreateBlobProcessor();
	auto pol_man = CreatePolynomialManager();
	
	
	// 1 Step
	int n_max; // ������������ ���������� ������� ���������.
	int diameter; // ������� ����������, �� ������� ����� ������������� ��������, �������.
	cout << "Enter polynomial count and radius:" << endl;
	cin >> n_max >> diameter;
	cout << endl;
    pol_man->InitBasis(n_max, diameter);

	// 2 Step
	ShowPolynomials("Polynomials", pol_man->GetBasis());
	

	// 3 Step
	cout << pol_man->GetType() << endl;

	// 4 Step
	auto image = imread("numbers.png", IMREAD_GRAYSCALE);
	imshow("src", image);
	
    // 5 Step

	//threshold(1, 1, 1, 1, 1); //�� �������� ���������
	//auto resultDB=blob_proc->DetectBlobs(image_bin); // ����� �������� ���a������������ �����������

	// 6 Step
	int side=5; // ������� �������� �� ������� ����� ���������� ��������������� ������� �������.
	//auto resultNB=blob_proc->NormalizeBlobs(resultDB,side);

	// 7 Step
	//cout << blob_proc->GetType() << endl;

	// 8 Step
	//auto resultD=pol_man->Decompose(image_bin); // ����� �������� ���a������������ �����������

	// 9 Step
	//auto resultR=pol_man->Recovery(resultD);

    // 10 Step
	//ShowBlobDecomposition("Str", image_bin,resultR);


	waitKey(0);



	return 0;
}